# Libguides column tweak

Adjust libguides column widths while preserving 25/75 styling

I modeled our Libguides CSS on our college styling; left-hand side is sidebar-like. This is fine so long as layout is 25/75 or 25/75/25, but any other layout looks bad.

This script changes the look based on the layout:

* if column 1 is set to more than 25%, the id changes, so the "sidebar" CSS no longer applies.
* if the layout is 75/25, column 3's id changes, so it gets the "sidebar" CSS.
* if the layout is 25/50/25 but the left column is empty, the layout switches to 10/80/10; application here is that content creators would only put content in column 2. Single-column looks better at 80% than it does at 100%.

I put this in our guide template because I wasn't sure it should affect the home page, A-to-Z etc.