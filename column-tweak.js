(function() {
// tweaks for SCC column layouts
var col1 = document.getElementById('s-lg-col-1');
var col2 = document.getElementById('s-lg-col-2');
// var col3 = document.getElementById('s-lg-col-3');
var hasBox = 's-lib-box';
var col1Class = col1.getAttribute('class');
if (col1.innerHTML.search(hasBox) === -1)
{ // if only column 2 is used, have it span 80% centered
    col1.setAttribute('class', 'col-md-1');
    col2.setAttribute('class', 'col-md-10');
}

else if ((col1Class === 'col-md-9') && (col2))
{ // if it's 75/25, create widget look in right column
    if (col2.getAttribute('class') === 'col-md-3')
    {
        col1.id = 'left-col';
        col2.id = 's-lg-col-1';
    }
}
else if (col1Class !== 'col-md-3')
{ // otherwise, if left is more than 25%, remove all widgetizing
    col1.id = 'left-col';
}
})();